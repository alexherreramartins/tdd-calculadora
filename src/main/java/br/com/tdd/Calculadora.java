package br.com.tdd;

public class Calculadora {
    public static int soma(int primeiroNumero, int segundoNumero) {
        return primeiroNumero + segundoNumero;
    }

    public static double soma(double primeiroNumero, double segundoNumero) {
        return primeiroNumero + segundoNumero;
    }

    public static double sub(double primeiroNumero, double segundoNumero) {
        return primeiroNumero - segundoNumero;
    }

    public static int sub(int primeiroNumero, int segundoNumero) {
        return primeiroNumero - segundoNumero;
    }

    public static double multiplicar(double primeiroNumero, double segundoNumero) {
        return primeiroNumero * segundoNumero;
    }

    public static int multiplicar(int primeiroNumero, int segundoNumero) {
        return primeiroNumero * segundoNumero;
    }

    public static double dividir(double primeiroNumero, double segundoNumero) {
        if (segundoNumero != 0.0){
            return primeiroNumero / segundoNumero;
        } else {
            throw new IllegalArgumentException("divisor deve ser diferente de zero!");
        }
    }

    public static int dividir(int primeiroNumero, int segundoNumero) {
        if (segundoNumero != 0){
            return primeiroNumero / segundoNumero;
        } else {
            throw new IllegalArgumentException("divisor deve ser diferente de zero!");
        }
    }
}
