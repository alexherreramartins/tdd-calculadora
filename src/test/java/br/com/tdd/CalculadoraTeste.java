package br.com.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class CalculadoraTeste {

    @Test
    public void TestarSomadeDoisNumerosInteiros(){
        int resultado = Calculadora.soma(2,2);

        Assertions.assertEquals(4,resultado);
    }

    @Test
    public void TestarSomadeDoisNumerosDouble(){
        double resultado = Calculadora.soma(2.0,2.0);

        Assertions.assertEquals(4.0,resultado);
    }

    @Test
    public void TestarSubtrairDoisNumerosInteiros(){
        int resultado = Calculadora.sub(2,1);

        Assertions.assertEquals(1,resultado);
    }

    @Test
    public void TestarSubtrairDoisNumerosDouble(){
        double resultado = Calculadora.sub(2.0,1.0);

        Assertions.assertEquals(1.0,resultado);
    }

    @Test
    public void TestarMultiplicarDoisNumerosInteiros(){
        int resultado = Calculadora.multiplicar(2,1);

        Assertions.assertEquals(2.0,resultado);
    }

    @Test
    public void TestarMultiplicarDoisNumerosDouble(){
        double resultado = Calculadora.multiplicar(2.0,1.0);

        Assertions.assertEquals(2.0,resultado);
    }

    @Test
    public void TestarDividirDoisNumerosInteiros(){
        int resultado = Calculadora.dividir(3,2);

        Assertions.assertEquals(1,resultado);
    }

    @Test
    public void TestarDividirDoisNumerosDouble(){
        double resultado = Calculadora.dividir(3.0,2.0);

        Assertions.assertEquals(1.5,resultado);
    }

    @Test
    public void TestarDividirPorZeroInteiro(){

       Exception exception =  assertThrows(IllegalArgumentException.class, () -> {
            Calculadora.dividir(3,0);
        });
    }

    @Test
    public void TestarDividirPorZeroDouble(){

        Exception exception =  assertThrows(IllegalArgumentException.class, () -> {
            Calculadora.dividir(3.0,0.0);
        });
    }

}
